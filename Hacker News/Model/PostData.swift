//
//  PostData.swift
//  Hacker News
//
//  Created by Ananth Chepuri on 13/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation

struct PostData: Decodable {
    let hits: [Post]
}

struct Post: Decodable, Identifiable {
    var id: String {
        return objectID
    }
    let objectID: String
    let points: Int
    let title: String
    let url: String?
}
