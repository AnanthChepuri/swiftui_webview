//
//  WebView.swift
//  Hacker News
//
//  Created by Ananth Chepuri on 13/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation
import WebKit
import SwiftUI

struct WebView: UIViewRepresentable {
    
    let urlString: String?
    
    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        if let safeString = urlString {
            if let safeURL = URL(string: safeString) {
                let request = URLRequest(url: safeURL)
                uiView.load(request)
            }
        }
    }
}

struct WebView_Previews: PreviewProvider {
    static var previews: some View {
        Text("Hello, World!")
    }
}
