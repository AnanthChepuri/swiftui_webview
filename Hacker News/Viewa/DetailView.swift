//
//  DetailView.swift
//  Hacker News
//
//  Created by Ananth Chepuri on 13/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import SwiftUI

struct DetailView: View {
    
    var url: String?
    
    var body: some View {
        WebView(urlString: url)
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(url: "WWW.google.com")
    }
}
